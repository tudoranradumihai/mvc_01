<?php

$categories = array(
1 => "Masini",
2 => "Ceasuri",
3 => "Motociclete"
	);

$priceRange = array(
1 => "sub 100",
2 => "100 - 200",
3 => "200 - 500",
4 => "peste 500"
);

$priceRange = array(
1 => array("name" => "sub 100", "query"=>"price < 100"),
2 => array("name" => "100 - 200", "query"=>"price >= 100 AND price < 200"),
3 => array("name" => "200 - 500", "query"=>"price >= 200 AND price < 500"),
4 => array("name" => "peste 500", "query"=>"price >= 500")
);

?>

<form id="search-form" action="index.php?C=Products&A=search" method="GET">
	<input type="hidden" name="C" value="<?php echo $_GET["C"] ?>" />
	<input type="hidden" name="A" value="<?php echo $_GET["A"] ?>" />
	<input type="text" id="category" />
	<?php foreach ($categories as $key => $category) { 
		echo '<a class="category-select" data-id="'.$key.'" href="#">'.$category.'</a>';
		if (array_key_exists("category", $_GET)){
			if ($key == $_GET["category"]) {
				echo '<a href="#" class="delete-category">(X)</a>';
			}
		} 
		echo "<br />";
		
		
	} ?>
	<input type="text" id="price" />
	<?php foreach ($priceRange as $key => $price) { 
		echo '<a class="price-select" data-id="'.$key.'" href="#">'.$price["name"].'</a>';
		if (array_key_exists("price", $_GET)){
			if ($key == $_GET["price"]) {
				echo '<a href="#" class="delete-price">(X)</a>';
			}
		} 
		echo "<br />";
		
		
	} ?>
	<input type="submit" />
</form>

<?php 
	$query = "SELECT * FROM products";

$filters = array();
if (array_key_exists("category", $_GET)){
	$filters[] = "category = ".$_GET["category"];
}
if (array_key_exists("price", $_GET)){
	$filters[] = $priceRange[$_GET["price"]]["query"];
}

if (count($filters)>0){
	$query .= " WHERE ".implode(" AND ", $filters);
}
echo $query;
?>

<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script>
$(document).ready(function(){

<?php
 if (array_key_exists("category", $_GET)){
?>
 	$("#category").attr("name","category");
 	$("#category").val(<?php echo $_GET["category"] ?>);
<?php 
 }
?>

<?php
 if (array_key_exists("price", $_GET)){
?>
 	$("#price").attr("name","price");
 	$("#price").val(<?php echo $_GET["price"] ?>);
<?php 
 }
?>

	$(".category-select").click(function(){
		$("#category").val($(this).attr("data-id"));
		$("#category").attr("name","category");
		$("#search-form").submit();
	});
	$(".delete-category").click(function(){
		$("#category").val("");
		$("#category").removeAttr("name");
		$("#search-form").submit();
	});
	$(".price-select").click(function(){
		$("#price").val($(this).attr("data-id"));
		$("#price").attr("name","price");
		$("#search-form").submit();
	});
	$(".delete-price").click(function(){
		$("#price").val("");
		$("#price").removeAttr("name");
		$("#search-form").submit();
	});
});
</script>