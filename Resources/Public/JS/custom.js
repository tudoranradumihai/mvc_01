$(document).ready(function(){
	$("#login").validate({
		onkeyup: false,
		rules : {
			email: {
				required: true,
				email: true,
				remote: "http://localhost/MVC_01/no-template.php?C=Users&A=checkEmailAddress"
			},
			password: {
				required: true,
				minlength: 8
			}
		},
		messages : {
			email: {
				remote: jQuery.validator.format("Email {0} doesn't exist in the database."),
			}
		},
		submitHandler: function(form) {
	      form.submit();
	    }
	});

	$("#users-new").validate({
		onkeyup: false,
		rules : {
			firstname: "required",
			lastname: "required",
			email: {
				required: true,
				email: true,
				remote: "http://localhost/MVC_01/no-template.php?C=Users&A=availableEmailAddress"
			},
			password: {
				required: true,
				minlength: 8
			},
			password2: {
				required: true,
				minlength: 8,
				equalTo: "#password"
			} 
		},
		messages : {
			email: {
				remote: jQuery.validator.format("Email {0} already exists in the database."),
			}
		},
		submitHandler: function(form) {
	      form.submit();
	    }
	});

	$("#users-edit").validate({
		onkeyup: false,
		rules : {
			firstname: "required",
			lastname: "required",
			image: {
				extension: "jpg|jpeg|png|gif"
			}
		},
		messages: {
			image: {
				extension: "The file must have one of the following extensions: jpg, jpeg, png, gif"
			}
		},
		submitHandler: function(form) {
	      form.submit();
	    }
	});

	$("#install").validate({
		onkeyup: false,
		rules : {
			hostname: "required",
			username: "required",
			database: "required"
		},
		submitHandler: function(form) {
	      form.submit();
	    }
	});
});