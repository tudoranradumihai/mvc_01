<ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
      <a href="<?php echo URLBuilder::create("Users","login") ?>" role="button" aria-haspopup="true" aria-expanded="false">
        Login
      </a>
    </li>
    <li class="dropdown">
      <a href="<?php echo URLBuilder::create("Users","new") ?>" role="button" aria-haspopup="true" aria-expanded="false">
        Register
      </a>
    </li>
  </ul>