<?php foreach ($errors as $error){ ?>
  <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
<?php } ?>
<div class="row">
  <div class="col-md-4 col-md-offset-4">
    <h1>Login</h1>
    <p>[TO DO]</p>
    <form id="login" method="POST" action="<?php echo URLBuilder::create('Users','connect');?>">
      <div class="form-group">
        <label>Email address</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $emailAddress; ?>">
      </div>
      <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>  
  </div>
</div>
