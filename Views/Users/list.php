<table class="table table-striped">
<thead>
<tr>
	<td>ID</td>
	<td>Firstname</td>
	<td>Lastname</td>
	<td>Email Address</td>
</tr>
</thead>
<?php foreach($users as $user) {?>
<tr>
	<td><?php echo $user->id; ?></td>
	<td><?php echo $user->firstname; ?></td>
	<td><?php echo $user->lastname; ?></td>
	<td><?php echo $user->email; ?></td>
	<td><a href="<?php echo URLBuilder::create('Users','show',$user->id) ?>">Show</a></td>
</tr>
<?php } ?>
</table>