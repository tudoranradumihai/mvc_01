<div class="alert alert-success" role="alert">A new account was created for the email <?php echo $user["email"];?>.</div>
<div class="row">
  	<div class="col-md-6 col-md-offset-3">
    	<h1>Registration</h1>
    	<p>You will be redirected to your profile in 5 seconds.</p>
    	<p><a href="<?php echo URLBuilder::create("Users","show",$user["id"]);?>">View My Profile</a></p>
    	<meta http-equiv='refresh' content='5; url=<?php echo URLBuilder::create("Users","show",$user["id"]);?>' />
	</div>
</div>