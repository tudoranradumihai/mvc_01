<?php foreach ($messages as $message){ ?>
  <div class="alert alert-success" role="alert"><?php echo $message; ?></div>
<?php } ?>
<?php foreach ($errors as $error){ ?>
  <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
<?php } ?>
<div class="row">
  	<div class="col-md-6 col-md-offset-3">
    	<h1>Edit Profile</h1>
    	<p><a href="<?php echo URLBuilder::create("Users","show",$user["id"]);?>">View My Profile</a></p>
    	<form class="form-horizontal" id="users-edit" action="<?php echo URLBuilder::create("Users","update"); ?>" method="POST" enctype="multipart/form-data">
	      <div class="form-group">
	        <label for="firstname" class="col-sm-2 control-label">Firstname</label>
	        <div class="col-sm-10">
	          <input type="text" class="form-control" id="firstname" placeholder="John" name="firstname" value="<?php echo $user["firstname"];?>">
	        </div>
	      </div>
	      <div class="form-group">
	        <label for="lastname" class="col-sm-2 control-label">Lastname</label>
	        <div class="col-sm-10">
	          <input type="text" class="form-control" id="lastname" placeholder="Doe" name="lastname" value="<?php echo $user["lastname"];?>">
	        </div>
	      </div>
	      <div class="form-group">
	        <label for="email" class="col-sm-2 control-label">Email Address</label>
	        <div class="col-sm-10">
	          <input type="email" class="form-control" id="email" readonly value="<?php echo $user["email"];?>">
	        </div>
	      </div>
	      <div class="form-group">
	        <label for="image" class="col-sm-2 control-label">Image</label>
	        <div class="col-sm-10">
	          <input type="file" class="form-control" id="image" name="image" >
	        </div>
	      </div>
	      <div class="form-group">
	        <div class="col-sm-offset-2 col-sm-10">
	          <button type="submit" class="btn btn-default">Edit</button>
	        </div>
	      </div>
	    </form>
	    <h2>Change your password</h2>
	    <form class="form-horizontal" id="users-edit-password" action="<?php echo URLBuilder::create("Users","update"); ?>" method="POST">
	      <div class="form-group">
	        <label for="oldpassword" class="col-sm-2 control-label">Old Password</label>
	        <div class="col-sm-10">
	          <input type="text" class="form-control" id="oldpassword" placeholder="Old Password" name="oldpassword">
	        </div>
	      </div>
	      <div class="form-group">
	        <label for="newpassword" class="col-sm-2 control-label">New Password</label>
	        <div class="col-sm-10">
	          <input type="text" class="form-control" id="newpassword" placeholder="New Password" name="newpassword">
	        </div>
	      </div>
	      <div class="form-group">
	        <label for="newpassword2" class="col-sm-2 control-label">Confirm New Password</label>
	        <div class="col-sm-10">
	          <input type="text" class="form-control" id="newpassword2" placeholder="New Password" name="newpassword2">
	        </div>
	      </div>
	      <div class="form-group">
	        <div class="col-sm-offset-2 col-sm-10">
	          <button type="submit" class="btn btn-default">Edit</button>
	        </div>
	      </div>
	    </form>
	</div>
</div>