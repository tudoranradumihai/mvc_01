<?php foreach ($errors as $error){ ?>
  <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
<?php } ?>
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <h1>Register</h1>
    <p>[TO DO]</p>
    <form class="form-horizontal" id="users-new" action="<?php echo URLBuilder::create("Users","create"); ?>" method="POST">
      <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">Firstname</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="firstname" placeholder="John" name="firstname">
        </div>
      </div>
      <div class="form-group">
        <label for="lastname" class="col-sm-2 control-label">Lastname</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="lastname" placeholder="Doe" name="lastname">
        </div>
      </div>
      <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email Address</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" id="email" placeholder="email@domain.com" name="email">
        </div>
      </div>
      <div class="form-group">
        <label for="password" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="password" placeholder="" name="password">
        </div>
      </div>
      <div class="form-group">
        <label for="password2" class="col-sm-2 control-label">Confirm Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="password2" placeholder="" name="password2">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Register</button>
        </div>
      </div>
    </form>
  </div>
</div>