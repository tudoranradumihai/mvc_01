<ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <img width="60" class="img-circle" src="http://localhost/MVC_01/Resources/Public/Uploads/Users/<?php echo $user["image"];?>" />
      </a>
      <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        Hello <?php echo $user["firstname"]." ".$user["lastname"]; ?> (<?php echo $user["email"] ?>)<span class="caret"></span>
      </a>
      <ul class="dropdown-menu">
	      <li><a href="<?php echo URLBuilder::create("Users","edit");?>">Edit My Profile</a></li>
        <li><a href="<?php echo URLBuilder::create("Friendship","management");?>">Manage Friend Requests</a></li>
	      <li><a href="<?php echo URLBuilder::create("Users","disconnect");?>">Logout</a></li>
	    </ul>
    </li>
  </ul>