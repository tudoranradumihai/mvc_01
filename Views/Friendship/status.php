<?php 
switch($status){
	case 1:
?>
<div class="btn-group">
  <button type="button" class="btn btn-default">Friends</button>
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo URLBuilder::create("Friendship","delete",$friendship->id); ?>">Unfriend</a></li>
  </ul>
</div>
<?php
	break;
	case 2:
?>
<div class="btn-group">
	<a href="<?php echo URLBuilder::create("Friendship","create",$_GET["UID"]); ?>">
	  <button type="button" class="btn btn-default">Add Friend</button>
	</a>
</div>
<?php
	break;
	case 3:
?>
<!-- Split button -->
<div class="btn-group">
  <button type="button" class="btn btn-default">Friend Request Sent</button>
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo URLBuilder::create("Friendship","delete",$friendship->id); ?>">Cancel Request</a></li>
  </ul>
</div>
<?php
	break;
	case 4:
?>
<!-- Split button -->
<div class="btn-group">
  <button type="button" class="btn btn-default">Friend Request Received</button>
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo URLBuilder::create("Friendship","accept",$friendship->id); ?>">Accept Friend Request</a></li>
    <li><a href="<?php echo URLBuilder::create("Friendship","delete",$friendship->id); ?>">Cancel Friend Request</a></li>
  </ul>
</div>
<?php
	break;
}
?> 
