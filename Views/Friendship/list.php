<h4>Friends (<?php echo count($users) ?>)</h4>
<div class="row">
<?php foreach ($users as $user) { ?>
<div class="col-md-2">
<a href="<?php echo URLBuilder::create("Users","show",$user->id);?>">
<?php echo $user->firstname." ".$user->lastname; ?>
</a>
</div>
<?php } ?>
</div>