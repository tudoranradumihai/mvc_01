<?php

Class Users {

	public $id;
	public $firstname;
	public $lastname;
	public $email;
	public $password;
	public $image;

	public function __construct($id, $firstname, $lastname, $email, $password, $image =NULL){
		$this->id 			= $id;
		$this->firstname 	= $firstname;
		$this->lastname 	= $lastname;
		$this->email 		= $email;
		$this->password 	= $password;
		$this->image 		= $image;
	}

}