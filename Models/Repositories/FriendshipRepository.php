<?php

Class FriendshipRepository extends Repository {

	public function findByUser($uid){
		$results = $this->database->executeQuery("SELECT * FROM friendship WHERE (user1=$uid OR user2=$uid) AND deleted=0 AND hidden=0 ORDER BY updated_date DESC");
		if (mysqli_num_rows($results)>0){
			$array = array();
			while ($item = mysqli_fetch_array($results,MYSQLI_ASSOC)){
				$object = new Friendship($item['id'],$item['deleted'],$item['hidden'],$item['user1'],$item['user2'],$item['created_date'],$item['updated_date']);
				array_push($array,$object);
			}
			return $array;
		} else {
			return NULL;
		}
	}

	public function requests($user,$type){
		if ($type=="IN"){
			$results = $this->database->executeQuery("SELECT * FROM friendship WHERE user2=$user AND deleted=0 AND hidden=1 ORDER BY created_date DESC");
		} else {
			$results = $this->database->executeQuery("SELECT * FROM friendship WHERE user1=$user AND deleted=0 AND hidden=1 ORDER BY created_date DESC");
		}
		if (mysqli_num_rows($results)>0){
			$array = array();
			while ($item = mysqli_fetch_array($results,MYSQLI_ASSOC)){
				$object = new Friendship($item['id'],$item['deleted'],$item['hidden'],$item['user1'],$item['user2'],$item['created_date'],$item['updated_date']);
				array_push($array,$object);
			}
			return $array;
		} else {
			return array();
		}
	}

	public function status($user1,$user2){
		$results = $this->database->executeQuery("SELECT * FROM friendship WHERE ((user1=$user1 AND user2=$user2) OR (user1=$user2 AND user2=$user1)) AND deleted=0 ORDER BY id DESC");
		if (mysqli_num_rows($results)>0){
			$item = mysqli_fetch_array($results,MYSQLI_ASSOC);
			$friendship = new Friendship($item['id'],$item['deleted'],$item['hidden'],$item['user1'],$item['user2'],$item['created_date'],$item['updated_date']);
			return $friendship;
		} else {
			return NULL;
		}

	}

	public function insert($object){
		$object = (array)$object;
		$fields = array();
		$values = array();
		foreach ($object as $key => $value){
			if($value){
				$fields[] = $key;
				$values[] = '"'.$value.'"';
			} else {
				unset($object[$key]);
			}
		}
		$fields = implode(", ",$fields);
		$values = implode(", ",$values);
		$result = $this->database->executeQuery("INSERT INTO friendship ($fields) VALUES ($values)");
		if($result){
			$result = $this->database->lastID();
		}
		return $result;
	}

	public function update($uid, $data){
		$values = array();
		foreach ($data as $key => $value){
			$values[] = $key."='".$value."'";
		}
		$values = implode(", ",$values);
		$result = $this->database->executeQuery("UPDATE friendship SET $values WHERE id=$uid");
		return $result;
	}

	public function softDelete($uid){
		$this->database->executeQuery("UPDATE friendship SET deleted=1 WHERE id=$uid");
	}

}