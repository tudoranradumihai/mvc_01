<?php

Class UsersRepository extends Repository {
	
	public function findAll(){
		$results = $this->database->executeQuery("SELECT * FROM users");
		if (mysqli_num_rows($results)>0){
			$array = array();
			while ($item = mysqli_fetch_array($results,MYSQLI_ASSOC)){
				$object = new Users($item["id"],$item["firstname"],$item["lastname"],$item["email"],$item["password"]);
				array_push($array,$object);
			}
			return $array;
		} else {
			return NULL;
		}
	}

	public function findByUID($UID){
		$results = $this->database->executeQuery("SELECT * FROM users WHERE id=".$UID);
		if (mysqli_num_rows($results)==1){
			$item = mysqli_fetch_array($results,MYSQLI_ASSOC);
			$object = new Users($item["id"],$item["firstname"],$item["lastname"],$item["email"],$item["password"],$item["image"]);
			return $object;
		} else {
			return NULL;
		}
	}


	public function findByUIDs($UIDs){
		foreach($UIDs as $key=>$value){
			$UIDs[$key] = "id=".$value;
		}
		$results = $this->database->executeQuery("SELECT * FROM users WHERE ".implode(" OR ",$UIDs));
		if (mysqli_num_rows($results)>0){
			$array = array();
			while ($item = mysqli_fetch_array($results,MYSQLI_ASSOC)){
				$object = new Users($item["id"],$item["firstname"],$item["lastname"],$item["email"],$item["password"]);
				array_push($array,$object);
			}
			return $array;
		} else {
			return NULL;
		}
	}

	public function checkCredentials($email,$password){
		$results = $this->database->executeQuery('SELECT * FROM users WHERE email LIKE "'.$email.'" AND password LIKE "'.md5($password).'"');
		if (mysqli_num_rows($results)==1){
			$item = mysqli_fetch_array($results,MYSQLI_ASSOC);
			$object = new Users($item["id"],$item["firstname"],$item["lastname"],$item["email"],$item["password"]);
			return $object;
		} else {
			return NULL;
		}
	}

	public function checkEmailAddress($email){
		$results = $this->database->executeQuery('SELECT * FROM users WHERE email LIKE "'.$email.'"');
		if (mysqli_num_rows($results)==1){
			$item = mysqli_fetch_array($results,MYSQLI_ASSOC);
			$object = new Users($item["id"],$item["firstname"],$item["lastname"],$item["email"],$item["password"]);
			return $object;
		} else {
			return NULL;
		}
	}

	public function insert($user){
		$user = (array)$user;
		$fields = array();
		$values = array();
		foreach ($user as $key => $value){
			if($value){
				$fields[] = $key;
				$values[] = '"'.$value.'"';
			} else {
				unset($user[$key]);
			}
		}
		$fields = implode(", ",$fields);
		$values = implode(", ",$values);
		$result = $this->database->executeQuery("INSERT INTO users ($fields) VALUES ($values)");
		if($result){
			$result = $this->database->lastID();
		}
		return $result;
	}

	public function update($uid, $data){
		$values = array();
		foreach ($data as $key => $value){
			$values[] = $key."='".$value."'";
		}
		$values = implode(", ",$values);
		$result = $this->database->executeQuery("UPDATE users SET $values WHERE id=$uid");
		return $result;
	}

	public function delete($UID){
		$this->database->executeQuery("DELETE FROM users WHERE id=".$UID);
	}

}