<?php

Class Friendship {

	public $id;
	public $deleted;
	public $hidden;
	public $user1;
	public $user2;
	public $created_date;
	public $updated_date;

	public function __construct($id, $deleted, $hidden, $user1, $user2, $created_date = NULL,$updated_date = NULL){
		$this->id 			= intval($id);
		$this->deleted 		= intval($deleted);
		$this->hidden 		= intval($hidden);
		$this->user1 		= $user1;
		$this->user2 		= $user2;
		$this->created_date = $created_date;
		$this->updated_date = $updated_date;
	}

}