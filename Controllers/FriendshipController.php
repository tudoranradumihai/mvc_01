<?php

Class FriendshipController extends Controller {

	private $friendshipRepository;

	public function __construct(){
		$this->friendshipRepository = new FriendshipRepository();
	}

	public function defaultAction(){

	}

	public function statusAction(){
		if (array_key_exists("user_uid", $_COOKIE)){
			if (intval($_GET["UID"])!=$_SESSION["user"]["id"]){
				$friendship = $this->friendshipRepository->status($_GET["UID"],$_SESSION["user"]["id"]);
				if (is_object($friendship)){
					if ($friendship->hidden==0){
						$status = 1; // You are friends 
					} else {
						if (intval($_SESSION["user"]["id"])==intval($friendship->user1)){
							$status = 3; // request sent 
						} else {
							$status = 4; // request received
						}
					}
				} else {
					$status = 2; // You can add this person
				}
			} else {
				$status = 0;// This is you
			}
			require "Views/Friendship/status.php";
		}
		
	}

	public function listAction(){
		if(!array_key_exists("UID", $_GET)){
			URLBuilder::redirect("Users","list");
		}
		$results = $this->friendshipRepository->findByUser($_GET["UID"]);
		if ($results){
			$temporary = array();
			foreach ($results as $item){
				if (intval($item->user1)==intval($_GET["UID"])){
					array_push($temporary,$item->user2);
				} else {
					array_push($temporary,$item->user1);
				}
			}
			$usersRepository = new usersRepository();
			$users = $usersRepository->findByUIDs($temporary);
		} else {
			$users = array();
		}
		require "Views/Friendship/list.php";
	}

	public function managementAction(){
		$usersRepository = new UsersRepository();
		$received = $this->friendshipRepository->requests($_SESSION["user"]["id"],"IN");
		foreach($received as $key=>$value){
			$received[$key] = (array)$value;
			$received[$key]["user1"] = (array)$usersRepository->findByUID($received[$key]["user1"]);
		}
		$sent = $this->friendshipRepository->requests($_SESSION["user"]["id"],"OUT");
		foreach($sent as $key=>$value){
			$sent[$key] = (array)$value;
			$sent[$key]["user2"] = (array)$usersRepository->findByUID($sent[$key]["user2"]);
		}
		require "Views/Friendship/management.php";
	}

	public function createAction(){
		if(!array_key_exists("user", $_SESSION)){
			URLBuilder::redirect("Users","list");
		}
		$friendship = new Friendship(
			NULL,
			0,
			1,
			$_SESSION["user"]["id"],
			$_GET["UID"]);
		$this->friendshipRepository->insert($friendship);
		URLBuilder::redirect("Users","show",$_GET["UID"]);
	}

	public function acceptAction(){
		if(!array_key_exists("user", $_SESSION)){
			URLBuilder::redirect("Users","list");
		}
		$this->friendshipRepository->update($_GET["UID"],array("hidden"=>0));
		if (array_key_exists("HTTP_REFERER", $_SERVER)){
			echo "<meta http-equiv='refresh' content='0; url=".$_SERVER["HTTP_REFERER"]."' />";
		}
	}

	public function deleteAction(){
		if(!array_key_exists("user", $_SESSION)){
			URLBuilder::redirect("Users","list");
		}
		$this->friendshipRepository->softDelete($_GET["UID"]);
		if (array_key_exists("HTTP_REFERER", $_SERVER)){
			echo "<meta http-equiv='refresh' content='0; url=".$_SERVER["HTTP_REFERER"]."' />";
		}
	}

}