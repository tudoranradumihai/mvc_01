<?php

Class UsersController extends Controller {

	private $usersRepository;

	public function __construct(){
		$this->usersRepository = new UsersRepository();
	}

	public function defaultAction(){
		self::listAction();
	} 

	public function listAction(){
		$users = $this->usersRepository->findAll();
		require "Views/Users/list.php";
	}

	public function showAction(){
		if (array_key_exists("UID", $_GET)){
			if (is_numeric($_GET["UID"])){
				$user = $this->usersRepository->findByUID(intval($_GET["UID"]));
				require "Views/Users/show.php";
			}
		} else {
			URLBuilder::redirect("Users","list");
		}
	}

	public function loginAction(){
		if(array_key_exists("user_uid", $_COOKIE)){
			URLBuilder::redirect("Users","show");
		}
		if(array_key_exists("errors-users-connect", $_SESSION)){
			$errors = $_SESSION["errors-users-connect"];
			unset($_SESSION["errors-users-connect"]);
		} else {
			$errors = array();
		}
		if(array_key_exists("user-data", $_SESSION)){
			$emailAddress = $_SESSION["user-data"]["email"];
			unset($_SESSION["user-data"]);
		} else {
			$emailAddress = "";
		}
		require "Views/Users/login.php";
	}

	public function connectAction(){
		if ($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = $this->usersRepository->checkCredentials($_POST["email"],$_POST["password"]);
			if (is_object($user)){
				setcookie("user_uid",$user->id,time()+3600,"/","http://localhost/MVC_01");
				$user = (array)$user;
				unset($user["password"]);
				$_SESSION["user"] = $user;
				URLBuilder::redirect("Users","show",$user["id"]);
			} else {
				$user = $this->usersRepository->checkEmailAddress($_POST["email"]);
				$_SESSION["errors-users-connect"][] = "The password for $_POST[email] is incorrect.";
				$user = (array)$user;
				unset($user["password"]);
				$_SESSION["user-data"] = $user;
				URLBuilder::redirect("Users","login");
			}
		} else {
			if (array_key_exists("HTTP_REFERER", $_SERVER)){
				echo "<meta http-equiv='refresh' content='0; url=".$_SERVER["HTTP_REFERER"]."' />"; // TO DO: 
			} else {
				URLBuilder::redirect("Users","login");
			}
		}
	}

	public function disconnectAction(){
		if (array_key_exists("user_uid", $_COOKIE)){
			setcookie("user_uid",NULL,time()-1);
		}
		if (array_key_exists("user", $_SESSION)){
			unset($_SESSION["user"]);
		}
		if (array_key_exists("HTTP_REFERER", $_SERVER)){
			echo "<meta http-equiv='refresh' content='0; url=".$_SERVER["HTTP_REFERER"]."' />"; // TO DO: 
		} else {
			echo "<meta http-equiv='refresh' content='0; url=".URLBuilder("Default","default")."' />";
		}
	}

	public function loginStatusAction(){
		if(array_key_exists("user_uid", $_COOKIE)){
			$user = $_SESSION["user"];
			require "Views/Users/loginStatusConnected.php";
		} else {
			require "Views/Users/loginStatus.php";
		}
	}

	public function checkEmailAddressAction(){
		if(array_key_exists("email", $_GET)){
			$user = $this->usersRepository->checkEmailAddress($_GET["email"]);
			if (is_object($user)){
				echo "true";
			} else {
				echo "false";
			}
		} else {
			echo "false";
		}
	}

	public function availableEmailAddressAction(){
		if(array_key_exists("email", $_GET)){
			$user = $this->usersRepository->checkEmailAddress($_GET["email"]);
			if (!is_object($user)){
				echo "true";
			} else {
				echo "false";
			}
		} else {
			echo "false";
		}
	}

	public function newAction(){
		if(array_key_exists("user_uid", $_COOKIE)){
			URLBuilder::redirect("Users","edit");
		}
		if(array_key_exists("errors-users-create", $_SESSION)){
			$errors = $_SESSION["errors-users-create"];
			unset($_SESSION["errors-users-create"]);
		} else {
			$errors = array();
		}
		require "Views/Users/new.php";
	}

	public function createAction(){
		if ($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = new Users(NULL,$_POST["firstname"],$_POST["lastname"],$_POST["email"],md5($_POST["password"]));
			$result = $this->usersRepository->insert($user);
			if($result){
				$user = $this->usersRepository->findByUID($result);
				setcookie("user_uid",$user->id,time()+3600,"/","http://localhost/MVC_01");
				$user = (array)$user;
				unset($user["password"]);
				$_SESSION["user"] = $user;
				require "Views/Users/create.php";
			} else {
				$_SESSION["errors-users-create"][] = "We are sorry but the registration has failed.";
				URLBuilder::redirect("Users","new");
			}
			
		} else {
			URLBuilder::redirect("Users","new");
		}
	}

	public function editAction(){
		if(!array_key_exists("user_uid", $_COOKIE)){
			URLBuilder::redirect("Users","list");
		} else {
			$user = $this->usersRepository->findByUID($_COOKIE["user_uid"]);
			$user = (array)$user;
			unset($user["password"]);
		}
		if(array_key_exists("messages-users-update", $_SESSION)){
			$messages = $_SESSION["messages-users-update"];
			unset($_SESSION["messages-users-update"]);
		} else {
			$messages = array();
		}
		if(array_key_exists("errors-users-update", $_SESSION)){
			$errors = $_SESSION["errors-users-update"];
			unset($_SESSION["errors-users-update"]);
		} else {
			$errors = array();
		}
		require "Views/Users/edit.php";
	}

	public function updateAction(){
		if ($_SERVER["REQUEST_METHOD"]=="POST"){
			if (!empty($_FILES["image"]["name"])){
				if (!file_exists("Resources/Public/Uploads")){
					mkdir("Resources/Public/Uploads");
				}
				if (!file_exists("Resources/Public/Uploads/Users")){
					mkdir("Resources/Public/Uploads/Users");
				}
				$extension = explode(".",$_FILES["image"]["name"]);
				$extension = $extension[count($extension)-1];
				$filename = $_COOKIE["user_uid"].".".$extension;
				if (file_exists("Resources/Public/Uploads/Users/".$filename)){
					unlink("Resources/Public/Uploads/Users/".$filename);
				}
				$result = move_uploaded_file($_FILES["image"]["tmp_name"], "Resources/Public/Uploads/Users/".$filename);
				if ($result){
					$_SESSION["messages-users-update"][] = "Your profile picture has been uploaded.";
					$this->usersRepository->update($_COOKIE["user_uid"],array("image"=>$filename));
				} else {
					$_SESSION["errors-users-update"][] = "We are sorry but we have encountered some issues. Your profile picture has not been uploaded. Please try again.";
				}
			}

			$result = $this->usersRepository->update($_COOKIE["user_uid"],$_POST);
			if ($result){
				$_SESSION["messages-users-update"][] = "Your profile has been updated.";
			} else {
				$_SESSION["errors-users-update"][] = "We are sorry but we have encountered some issues. Your profile has not been updated. Please try again.";
			}
			$user = (array)$this->usersRepository->findByUID($_COOKIE["user_uid"]);
			unset($user["password"]);
			$_SESSION["user"] = $user;
		}
		URLBuilder::redirect("Users","edit");
	}

	public function deleteAction(){
		if (array_key_exists("UID", $_GET)){
			if (is_numeric($_GET["UID"])){
				$this->usersRepository->delete(intval($_GET["UID"]));
				echo '<meta http-equiv="refresh" content="0; url=index.php?C=Users&A=list" />';
				
			}
		}
	}

}