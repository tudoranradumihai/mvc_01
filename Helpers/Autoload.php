<?php

function __autoload($class){
	$folders = array(
		"Controllers",
		"Helpers",
		"Models",
		"Models/Repositories"
	);
	foreach ($folders as $folder) {
		$filepath = $folder."/".$class.".php";
		if (file_exists($filepath)){
			require_once $filepath;
		}
	}
}