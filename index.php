<?php
session_start();
require "Helpers/Initialize.php";
require "Helpers/Autoload.php";
?>
<!DOCTYPE HTML>
<html>
	<head>
		<?php require "Resources/Public/Partials/HeaderResources.php"; ?>
	</head>
	<body>
		<div class="container">
		<?php require "Resources/Public/Partials/Header.php"; ?>
		<?php initialize(); ?>
		<?php require "Resources/Public/Partials/Footer.php"; ?>
		</div>
		<?php require "Resources/Public/Partials/FooterResources.php"; ?>
	</body>
</html>