<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$configuration = array(
		"GENERAL" => array(
			"DOMAIN"		=> $_POST["domain"],
			"REWRITE_MODE"	=> true,
			"ABSOLUTE_PATH"	=> true
		),
		"DATABASE" => array(
			"HOSTNAME" => $_POST["hostname"],
			"USERNAME" => $_POST["username"],
			"PASSWORD" => $_POST["password"],
			"DATABASE" => $_POST["database"]
		)
	);
	$handle = fopen("Configuration/EnvironmentConfiguration.php","w");
	fputs($handle,"<?php".PHP_EOL);
	fputs($handle,"return '".json_encode($configuration)."';");
	fclose($handle);
}
if (file_exists("Configuration/EnvironmentConfiguration.php")){
$handle = fopen("Configuration/EnvironmentConfiguration.php","r");
$temporary = fgets($handle);
$temporary = fgets($handle);
$temporary = explode("'",$temporary);
$configuration = json_decode($temporary[1],TRUE);
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<?php require "Resources/Public/Partials/HeaderResources.php"; ?>
	</head>
	<body>
		<div class="container">
		<h1>Install Project</h1>
<?php
if ($configuration["DATABASE"]["HOSTNAME"]!="" && $configuration["DATABASE"]["USERNAME"]!="") {
$databaseConnection = @mysqli_connect($configuration["DATABASE"]["HOSTNAME"],$configuration["DATABASE"]["USERNAME"],$configuration["DATABASE"]["PASSWORD"],$configuration["DATABASE"]["DATABASE"]);
$databaseResponse = mysqli_connect_error();
	if($databaseResponse){
		echo '<div class="alert alert-danger" role="alert"><b>Error</b>: '.$databaseResponse.'</div>';
	} else {
		echo '<div class="alert alert-success" role="alert"><b>Success</b>: Database Connected</div>';
		$tables = scandir("Resources/Private/SQL");
		foreach ($tables as $table){
			if(strpos($table,".sql")!==false){
				$temporary = explode(".",$table);
				$result = mysqli_query($databaseConnection,"SHOW TABLES LIKE '".$temporary[0]."'");
				if(mysqli_num_rows($result)==0){
					mysqli_query($databaseConnection,file_get_contents("Resources/Private/SQL/".$table));
				}
			}
		}
	}
} else {
	echo '<div class="alert alert-warning" role="alert"><b>Warning</b>: No Database Information</div>';
}
?>		<form class="form-horizontal" method="POST" id="install">
			
			<div class="col-md-6">
			<h3>General Configuration</h3>
			<div class="form-group">
			    <label for="domain" class="col-sm-2 control-label">Domain</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="domain" name="domain" placeholder="Domain" value="<?php if(isset($configuration)){echo $configuration["GENERAL"]["DOMAIN"];} ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="domain" class="col-sm-2 control-label"></label>
			    <div class="col-sm-10">
			      <input type="checkbox" name="rewrite"> Rewrite Links
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="domain" class="col-sm-2 control-label"></label>
			    <div class="col-sm-10">
			      <input type="checkbox" name="absolute"> Include Absolute Path
			    </div>
			  </div>
  			</div>
			<div class="col-md-6">
			<h3>Database Configuration</h3> 
			
			  <div class="form-group">
			    <label for="hostname" class="col-sm-2 control-label">Hostname</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="hostname" name="hostname" placeholder="Hostname" value="<?php if(isset($configuration)){echo $configuration["DATABASE"]["HOSTNAME"];} ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="username" class="col-sm-2 control-label">Username</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php if(isset($configuration)){echo $configuration["DATABASE"]["USERNAME"];} ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="password" class="col-sm-2 control-label">Password</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="password" name="password" placeholder="Password" value="<?php if(isset($configuration)){echo $configuration["DATABASE"]["PASSWORD"];} ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="database" class="col-sm-2 control-label">Database</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="database" name="database" placeholder="Database" value="<?php if(isset($configuration)){echo $configuration["DATABASE"]["DATABASE"];} ?>">
			    </div>
			  </div>
			
			</div>

			<div class="col-md-12">
				<div class="pull-right">
					<button type="submit" class="btn btn-default">Submit</button>
				</div>
			</div>
			</form>
		</div>

		<?php require "Resources/Public/Partials/FooterResources.php"; ?>
	</body>
</html>